<?php

/**
 * LaraClassified - Classified Ads Web Application
 * Copyright (c) BedigitCom. All Rights Reserved
 *
 * Website: https://bedigit.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
 */

namespace App\Traits;

use App\Models\RfidCard;
use App\Models\User;

trait UserTrait
{

    public function assignCardToUser($token, $user)
    {
        // dd($user_id);
        $card = RfidCard::where('token', $token)->first();
        // dd($card);
        // dd($user_id);
        // dd($user);
        $user->rfid_id = $card->id;
        $user->save();

        // $card->hash_token = md5($user->national_id);
        $card->save();

        return true;
    }
    public function removeCardFromUser($user_id)
    {
        $user = User::find($user_id);
        $user->rfid_id = NULL;
        $user->save();
        return true;
    }

    public function removeUserCard($user)
    {
        // dd($user->userInfo);

        $rfidCard = $user->card;
        if ($rfidCard != NULL)
        {
            $rfidCard->hash_token = NULL;
            $rfidCard->save();
        }

        return true;
    }

    public function checkUserExist($email)
    {
        $user = User::where('email', $email)->first();
        // dd($user);
        if ($user == null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public function checkUserExistCnic($cnic)
    {
        $user = User::where('cnic', $cnic)->first();
        // dd($user);
        if ($user == null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public function getUserByRfidCard($token)
    {
        $card = RfidCard::with('user')->where('token', $token)->first();
        return $card->user;
    }




    // public function assignRfidToUser($token, $userID, $role)
    // {
    //     $card = RfidCard::where('token', $token)->first();
    //     // dd($card);
    //     if ($card != null)
    //     {
    //         if ($card->user != null)
    //         {
    //             Alert::error('Error', 'Card already assigned to user');
    //             // return redirect()->back()->with('error', 'Card already assigned to user');
    //             return false;
    //         }
    //     }

    //     $user = User::find($userID);

    //     $rdifCard = new RfidCard();
    //     $rdifCard->token = $token;
    //     $rdifCard->hash_token = md5($user->national_id);
    //     $rdifCard->save();


    //     $user->rfid_id = $rdifCard->id;
    //     $user->save();

    //     $user->assignRole($role);

    //     Alert::success('Success', 'Card Assigned to user');
    //     return true;
    // }


    // public function CheckCardExist($token)
    // {
    //     $card = RfidCard::where('token', $token)->first();
    //     if ($card == null)
    //     {
    //         return false;
    //     }
    //     else
    //     {
    //         return true;
    //     }
    // }
    // public function CheckCardAssignToOtherUser($token)
    // {
    //     $card = RfidCard::where('token', $token)->first();
    //     if ($card->user == null)
    //     {
    //         return false;
    //     }
    //     else
    //     {
    //         return true;
    //     }
    // }
    // public function createRfidCard($token)
    // {
    //     $card = new RfidCard();
    //     $card->token = $token;
    //     $card->save();
    //     return true;
    // }

    // public function assignCardToUser($token, $user_id)
    // {
    //     $card = RfidCard::where('token', $token)->first();
    //     $user = User::find($user_id);
    //     // dd($user->userInfo);
    //     $user->rfid_id = $card->id;
    //     $user->save();

    //     $card->hash_token = md5($user->national_id);
    //     $card->save();

    //     return true;
    // }
}
