<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RfidCard extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->hasOne(User::class, 'rfid_id');
    }
}
