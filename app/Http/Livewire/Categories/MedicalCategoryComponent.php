<?php

namespace App\Http\Livewire\Categories;

use App\Models\MedicalCategory;
use Livewire\Component;
use Livewire\WithPagination;

class MedicalCategoryComponent extends Component
{


    use WithPagination;

    public $heading = "Medical Category";
    public $createAllow = false;
    public $searchInput = [];
    public MedicalCategory $model;

    public function mount()
    {
        $this->createNewInstance();
    }


    protected $rules = [
        'model.name' => 'required',
    ];

    public function createNewInstance()
    {
        // dd('here');
        $this->model = new MedicalCategory();
    }

    public function render()
    {
        return view('livewire.categories.medical-category-component')
            ->with(
                [
                    'data' => $this->getData()
                ]
            )
            ->extends('layouts.app', ['activePage' => 'medical_category']);;
    }

    public function getData()
    {
        return MedicalCategory::orderByDesc('updated_at')
            ->when($this->searchInput['name'] ?? false, function ($q)
            {
                $q->where('name', 'like', search($this->searchInput['name']));
            })
            ->paginate(10);
    }

    public function showCreate()
    {
        $this->createAllow = true;
        $this->createNewInstance();
    }

    public function save()
    {

        $this->validate();

        $this->model->save();

        $this->dispatchBrowserEvent('swal:modal', [
            'type' => 'success',
            'title' => 'Success',
            'text' => 'Created',
        ]);

        $this->resetExcept('model');
    }

    public function edit(MedicalCategory $model)
    {
        $this->model = $model;
    }
    public function update()
    {
        $this->validate();
        $this->model->update();
        $this->emit('closeModal');
    }

    public function destroy(MedicalCategory $model)
    {
        $model->delete();
    }
}
