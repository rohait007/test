<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CarResource;
use App\Models\Car;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CarController extends Controller
{
    protected $rules = [
        'category_id' => 'required',
        'color'       => 'required|regex:/^[ A-Za-z0-9]+$/u',
        'model'       => 'required|regex:/^[ A-Za-z0-9]+$/u',
        'reg_number'  => 'required|regex:/^[ A-Za-z0-9]+$/u',
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page_size = $request->page_size ?? 10;
        return (new CarResource(Car::paginate($page_size)))
            ->response()
            ->setStatusCode(200);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), $this->rules);

        if ($validator->fails())
        {
            return response(array(
                'message' => $validator->errors(),
            ), 400);
        }
        else
        {

            $category = Category::find($request->category_id);
            if (!$category)
            {
                return response(array(
                    'message' => 'Category id invalid',
                ), 404);
            }

            $model =  Car::create(
                $request->all()
            );

            if ($model)
            {
                return (new CarResource($model))
                    ->response()
                    ->setStatusCode(200);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Car::find($id);
        if (!$model)
        {
            return response(array(
                'message' => 'Not Found',
            ), 404);
        }
        else
        {
            return (new CarResource($model))
                ->response()
                ->setStatusCode(200);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $model = Car::find($id);

        if (!$model)
        {
            return response(array(
                'message' => 'Not Car Found',
            ), 404);
        }


        $validator = Validator::make($request->all(), $this->rules);

        if ($validator->fails())
        {
            return response(array(
                'message' => $validator->errors(),
            ), 400);
        }
        else
        {
            if ($model->update($request->all()))
            {
                return (new CarResource($model))
                    ->response()
                    ->setStatusCode(200);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Car::find($id);

        if (!$model)
        {
            return response(array(
                'message' => 'Not Found',
            ), 404);
        }

        if ($model->delete())
        {
            return (new CarResource($model))
                ->response()
                ->setStatusCode(200);
        }
    }
}
