<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\RegisterResource;
use App\Jobs\SendMail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;


class RegisterController extends Controller
{



    public function register(Request $request)
    {


        $rules = array(
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        );

        $validator = Validator::make($request->all(), $rules);


        if ($validator->fails())
        {
            return response(array(
                'message' => $validator->errors(),
            ), 400);
        }
        else
        {
            $randomPassword = Str::random(8);

            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($randomPassword),
            ]);


            if ($user != null)
            {
                $data['email'] = $user->email;
                $data['name'] = $user->name;
                $data['password'] = $randomPassword;
                $data['url'] = env('APP_URL') . '/login';

                dispatch(new SendMail($data));
            }


            return (new RegisterResource($user))
                ->response()
                ->setStatusCode(200);
        }
    }
}
