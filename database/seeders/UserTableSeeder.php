<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // User::factory()->count(20)->create();

        DB::table('users')->insert([

            [
                'name' => 'Admin',
                'email' => 'admin@hms.com',
                'password' => hash::make('hospital'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Editor',
                'email' => 'editor@hms.com',
                'password' => hash::make('hospital'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],

        ]);

        $users = User::all();

        foreach ($users as $user)
        {
            if ($user->name == 'Admin')
            {
                $user->assignRole('admin');
            }

            if ($user->name == 'Editor')
            {
                $user->assignRole('editor');
            }
        }

        // User::factory(50)->create()->each(function ($user)
        // {
        //     $user->assignRole('patient'); // assuming 'supscription' was a typo
        // });
    }
}
