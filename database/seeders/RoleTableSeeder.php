<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        Role::create(
            ['name' => 'admin']
        );
        Role::create(
            ['name' => 'editor']
        );
        Role::create(
            ['name' => 'patient']
        );
        Role::create(
            ['name' => 'doctor']
        );
        Role::create(
            ['name' => 'staff']
        );
    }
}
