        <div class="app-sidebar">
            <div class="logo">
                <a href="/" class="logo-icon">
                    <span class="logo-text">{{ 'HMS' }}
                    </span>
                </a>
                <div class="sidebar-user-switcher user-activity-online">
                    <a href="#">
                        <img src="{{ asset('images/avatars/avatar.png') }}">
                        <span class="activity-indicator"></span>
                        <span class="user-info-text">{{ auth()->user()->name ?? '' }}<br><span class="user-state-info"></span></span>
                    </a>

                </div>
            </div>

            <div class="app-menu">

                <ul class="accordion-menu">

                    <li class="sidebar-title">
                        Apps
                    </li>
                    <li class="{{ $activePage == 'dashboard' ? 'active-page' : '' }} ">
                        <a href="/" class="active"><i class="material-icons-two-tone">dashboard</i>Dashboard</a>
                    </li>

                    <li class="{{ $activePage == 'category' ? 'active-page' : '' }} ">
                        <a href="{{ route('category') }}">
                            <i class="material-icons-two-tone">category</i>Add Car Category
                        </a>
                    </li>

                    <li class=" {{ $activePage == 'car' ? 'active-page' : '' }} ">
                        <a href="{{ route('car') }}">
                            <i class="material-icons-two-tone">directions_car</i>Cars</a>
                    </li>

                    <li class="sidebar-title">
                        Actions
                    </li>

                    <li class="nav-item">

                        <a class="" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                                                                                                                                                                                                                                                                                        document.getElementById('logout-form').submit();">
                            <i class="material-icons-two-tone">logout</i> {{ 'Logout' }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </li>
                </ul>
                <div class="d-lg-none float-end">
                    <livewire:view-mode.view-mode-component>
                </div>

            </div>

        </div>
