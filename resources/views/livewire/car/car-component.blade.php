<div>
    <div class="row mb-2">
        <div class="col-md-4 offset-md-8">
            <div class="float-end">
                @if ($createAllow)
                    <x-add-btn wire:click="$set('createAllow', false)">Back</x-add-btn>
                @else
                    <x-add-btn wire:click='showCreate'>Add</x-add-btn>
                @endif
            </div>
        </div>
    </div>

    @if ($createAllow)
        <div class="card p-3">
            <div class="row">
                <div class="col-12 col-md-8 mx-auto">
                    <form class="g-3" wire:submit.prevent="{{ $route }}">
                        <div class="row my-4">
                            <div class="">
                                <label for="name" class="form-label">Category</label>
                                <select wire:model.defer='model.category_id' class="form-select">
                                    <option value="" selected>Select Category</option>
                                    @foreach ($categories as $row)
                                        <option value="{{ $row->id }}" selected>{{ $row->name }}</option>
                                    @endforeach
                                </select>

                                @error('model.name')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="row my-4">
                            <div class="col-12 col-md-6">
                                <label for="color" class="form-label">Color</label>
                                <input wire:model.defer='model.color' type="text" class="form-control" placeholder="Red" required>

                                @error('model.color')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-12 col-md-6">
                                <label for="model" class="form-label">Car Model</label>
                                <input wire:model.defer='model.model' type="text" class="form-control" placeholder="Car Model" required>

                                @error('model.model')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>

                        </div>
                        <div class="row my-4">
                            <div class="col-12 col-md-6">
                                <label for="reg_number" class="form-label">Registration</label>
                                <input wire:model.defer='model.reg_number' type="text" class="form-control" placeholder="LXF 9666" required>

                                @error('model.reg_number')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>

                        </div>

                        <div class="row">
                            <div class="d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    @else
        @if ($poll)
            <div wire:poll.1ms="reloadDataTable">

            </div>
        @endif

        <div class="card p-3">
            @if ($showDatatable)
                <livewire:car-table />
            @endif
        </div>

    @endif

</div>
