<a {{ $attributes->merge(['class' => 'btn btn-sm btn-primary']) }}>
    <x-icon class="fas fa-link"></x-icon>
    <span class="d-none d-sm-inline ml-1"> {{ $slot }}</span>
</a>
