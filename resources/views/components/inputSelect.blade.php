<select wire:model='searchInput.{{ $model }}' class="form-select rounded-0 select2Cus" attr={{ $model }} tabindex="-1" style="width: 100%" required>
    {{ $slot }}
</select>
